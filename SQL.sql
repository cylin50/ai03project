USE MYDB;
CREATE USER 'gary'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Aa111111';
GRANT ALL PRIVILEGES ON MYDB.* To 'gary'@'localhost';

DROP TABLE MYDB.USER;
CREATE TABLE MYDB.USER (
	USERID varchar(40) not null COMMENT 'USER UUID',
    USER_NM varchar(500) not null COMMENT '客戶姓名',
    FACE_VECT BLOB not null comment '臉部向量',
    STATUS boolean default true not null comment '狀態'
);

CREATE TABLE MYDB.ROOM (
	USERID varchar(40) not null COMMENT 'USER UUID',
    ROOMID varchar(10) not null COMMENT '房號',
    CRE_DT date not null COMMENT '入住日期',
	END_DT date not null COMMENT '退房日期',
    STATUS boolean default true not null comment '狀態'
);

SELECT *
FROM MYDB.USER A;

SELECT USER_NM, FACE_VECT, ROOMID 
FROM MYDB.USER A
INNER JOIN MYDB.ROOM B ON A.USERID = B.USERID
WHERE ROOMID = '306';
