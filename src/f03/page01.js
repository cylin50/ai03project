const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');
const http = require('http');

/**GET */
router.get('/page01', (req, res) => {
    res.render('f03/page01');
});

/**Post */
/**範例 */
router.post('/voice2Word', (req, res) => {
    console.log('voice2Word');
    /**設定/讀取/參數 */
    let resend = {};
    /**參數判斷 */
    
    var options = {
        host: '127.0.0.1',
        port: 5001,
        path: '/api/voice2Word',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            // 'Content-Length': Buffer.byteLength(sendData)
        }
    };

    api = http.request(options, function(resData) {
        console.log('STATUS: ' + resData.statusCode);
        console.log('HEADERS: ' + JSON.stringify(resData.headers));
        resData.setEncoding('utf8');
        resData.on('data', function (chunk) {
            // console.log('BODY: ' + chunk);
            resend.voicestr = chunk.toString();
        });
        resData.on('end', () => {
            resend.code = 200;
            res.json(resend);
        });
    });

    api.write("");

    api.end();
});

module.exports = router;