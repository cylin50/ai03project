const express = require('express');
const url = require('url');
const bodyParser = require('body-parser');
const session = require('express-session'); 
const moment = require('moment-timezone');
const db = require(__dirname + '/core/db_connection');
const camera = require(__dirname + '/core/camera');
const fs = require("fs");

/**建立web Server物件 */
var app = express();
/**設定Port */
var server = app.listen(3000, () => { console.log('Server Start Port:3000'); });
var io = require('socket.io')(server);

/**設定ejs頁面位置，非預設位置改為  app.set('views', '路徑'); */
app.set('view engine', 'ejs');

/**設定靜態頁面位置，非預設位置改為  app.use(express.static('路徑')); */
app.use(express.static('public'));
/**將body-parser設定在top middleware */
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
/**設定session參數 */
app.use(session({
    saveUninitialized: false,
    resave: false,
    secret:'asddaassddassd',
    cookie: {
        maxAge: 600000
    }
}));

/**載入外部連結 */
const def = require(__dirname + '/core/def');
app.use(def);

const test = require(__dirname + '/test');
app.use('/test', test);

const f01_page01 = require(__dirname + '/f01/page01');
app.use('/f01', f01_page01);
const f01_page02 = require(__dirname + '/f01/page02');
app.use('/f01', f01_page02);

const f02_page01 = require(__dirname + '/f02/page01');
app.use('/f02', f02_page01);

const f03_page01 = require(__dirname + '/f03/page01');
app.use('/f03', f03_page01);

const f04_page01 = require(__dirname + '/f04/page01');
app.use('/f04', f04_page01);

const f05_page01 = require(__dirname + '/f05/page01');
app.use('/f05', f05_page01);


/**設定路由 */
app.get('/', (req, res) => {
    res.render('index');
});

setInterval(() => {
    fs.readFile('public/images/data.txt', function (err, data) {
        result = {}
        result.data = data.toString()
        result.code = 200
        // console.log(result)
        io.emit('image', result);
    });
}, 100)

/**設定404頁面 */
app.use((req, res) => {
    res.type('text/plain');
    res.status(404);
    res.send('404 - 找不到網頁');
});

