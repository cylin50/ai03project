const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');

/**GET */
router.get('/page01', (req, res) => {
    res.render('f04/page01');
});

/**Post */
/**範例 */
router.post('/getCommList', (req, res) => {
    console.log('getCommList');
    /**設定/讀取/參數 */
    const postData = req.body;
    let resend = {};
    /**參數判斷 */
    let where = 'WHERE CRE_USER = ? ';
    let params = [userId];
    if (postData.commNm) { 
        where += `AND COMM_NM Like ? `;
        params.push('%' + postData.commNm + '%');
    }
    if (postData.vendor) { 
        where += `AND VEN_ID = ? `;
        params.push(postData.vendor);
    }
    

    /**SQL判斷 */
    let sql1 = `SELECT COUNT(1) count FROM INMA_COMM ${where} `;
    db.queryAsync(sql1, params)
        .then(result => {
            resend.total = result[0].count;
            let sql2 = `SELECT A.COMM_ID, 
                               A.COMM_NM,
                               A.UNIT,
                               A.VEN_ID, 
                               (SELECT VEN_NM FROM INMA_VENDOR WHERE VEN_ID = A.VEN_ID) AS VEN_NM, 
                               A.STATUS, 
                               (SELECT CD_NM FROM SYS_CD WHERE CT_ID = '01' AND CD_ID = A.STATUS) AS ST_NM
                        FROM INMA_COMM A ${where} ORDER BY ${sort} ${order} LIMIT ${start}, ${rows}`;
            return db.queryAsync(sql2, params);
        })
        .then(result => {
            resend.rows = result;
            res.json(resend);
        });
});

module.exports = router;