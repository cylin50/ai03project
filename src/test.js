const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/core/db_connection');

const utils = require(__dirname + '/core/utils');

router.get('/video_feed', (req, res) => {
    return Response(generate(),
		mimetype = "multipart/x-mixed-replace; boundary=frame")
});

router.get('/testimport', (req, res) => {
    console.log(utils.f1(1,2));
    console.log(utils.f2(3,4));
    res.json(utils.f1());
});

router.get('/sender', (req, res) => {
    const urlParm = url.parse(req.url, true);
    const data = urlParm.query;
    console.log(urlParm);
    res.render('sender');
});

router.post('/sender', (req, res) => {
    const urlParm = url.parse(req.url, true);
    const data = urlParm.query;
    console.log(urlParm);
    res.render('sender');
});

router.get('/stockReport', (req, res) => {
    
    res.render('f01/stockReport');
});

router.post('/stockReport', (req, res) => {
    res.render('f01/stockReport');
});

router.get('/check', (req, res) => {
    const urlParm = url.parse(req.url, true);
    const data = urlParm.query;
    console.log(data);
    res.render('test');
});

router.post('/check', (req, res) => {
    const postData = req.body;
    console.log(postData)
    
    res.render('test');
});


/**設定Post方法 */
router.post('/getdata/', (req, res) => {
    const data = req.body;
    console.log(data);
    res.json(data);
});

router.get('/querydb', (req, res) => {
    let sql = `SELECT * FROM INMA_VENDOR`;
    db.queryAsync(sql)
        .then(result => {
            res.json(result);
        });
});



module.exports = router;