const mysql = require('mysql');
const bluebird = require('bluebird');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'gary',
    password: 'Aa111111',
    database: 'MYDB'
});
bluebird.promisifyAll(db);

module.exports = db;