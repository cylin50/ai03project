module.exports = {
    getbase64Img: (file) =>{
        var fs = require('fs');
        var bitmap = fs.readFileSync(file);
        return new Buffer(bitmap).toString('base64');
    }
};