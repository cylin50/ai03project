import sys 
import tensorflow as tf
import numpy as np
import scipy.misc
from cv2 import cv2 as cv2
import facenet
import face_recognition
import imutils
import json
from flask import Flask, request, abort
import requests
import uuid
import mysql.connector
import base64
import dlib
from imutils.face_utils import FaceAligner
from imutils import face_utils

# cust import
import voice2Text as vt
import filetranslate as ft
import AutoSpeaking as As

def loadfacenet(modeldir):
    tf.Graph().as_default()
    sess = tf.Session()

    facenet.load_model(modeldir)
    images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
    embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
    phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
    embedding_size = embeddings.get_shape()[1]

    return sess, embeddings, embedding_size, images_placeholder, phase_train_placeholder

app = Flask(__name__)
sess, embeddings, embedding_size, images_placeholder, phase_train_placeholder = loadfacenet('./facenetModel/20180402-114759.pb')
host = "127.0.0.1"
port = 3306
database = 'mydb'
user = "gary"
password = "Aa111111"

# routes ====================

@app.route("/api/face2vect", methods=['POST'])
def face2vect():
    print('face2vect')
    # imagePath = str(request.form.get('imagePath'))
    base64_str = str(request.form.get('base64_str'))
    rectangle = json.loads(request.form.get('rectangle'))
    print("rectangle:{}".format(rectangle))
    roomId = str(request.form.get('roomId'))
    userNm = str(request.form.get('userNm'))

    emb_array = getImgVect(base64_str, rectangle)
    
    data = ",".join([str(x) for x in emb_array])
    result = "[" + data + "]"

    insert2DB(result, roomId, userNm)

    return result

@app.route("/api/faceUnlock", methods=['POST'])
def faceUnlock():
    base64_str = str(request.form.get('base64_str'))
    rectangle = json.loads(request.form.get('rectangle'))
    roomId = str(request.form.get('roomId'))

    emb_array = getImgVect(base64_str, rectangle)
    vectlist = loadFromDB(roomId)
    finalresult = Odist(emb_array, vectlist)

    return finalresult

@app.route("/api/voice2Word", methods=['POST'])
def voice2Word():
    
    vt.record_to_file()
    result = ft.translate()

    return result

@app.route("/api/AutoSpeaking", methods=['POST'])
def AutoSpeaking():
    result=As.read()
    return result

# functions ===================

def getImgVect(base64_str, rectangle):
    image_size = 160

    imgString = base64.b64decode(base64_str)
    nparr = np.fromstring(imgString,np.uint8)  
    image = cv2.imdecode(nparr,cv2.IMREAD_COLOR)

    # image = cv2.imread(imagePath, cv2.COLOR_BGR2RGB)
    img = preProcessingImgAligner(image)
    img = preProcessingImg(img, rectangle)
    
    # img = cv2.imread(imagePath, cv2.COLOR_BGR2RGB)
    print('cv2.imread')
    scaled_reshape = []
    image = cv2.resize(img, (image_size, image_size), interpolation=cv2.INTER_CUBIC)
    image = facenet.prewhiten(image)
    print('facenet.prewhiten')
    scaled_reshape.append(image.reshape(-1,image_size,image_size,3))
    emb_array = np.zeros((1, embedding_size))
    emb_array[0, :] = sess.run(embeddings, feed_dict={images_placeholder: scaled_reshape[0], phase_train_placeholder: False })[0]
    print('emb_array')
    
    return emb_array[0]

def base642vect():
    return ''

# 圖片前處理
def preProcessingImg(img, rectangle):
    image = img.copy()
    
    top = int(rectangle['top'])
    bottom = int(rectangle['bottom'])
    left = int(rectangle['left'])
    right = int(rectangle['right'])
    
    crop_img = image[top:bottom, left:right]
    blur_image = cv2.bilateralFilter(crop_img, 9, 41, 41)
    image_light = cv2.addWeighted(blur_image, 1.2, blur_image, -0.2, 20)

    return crop_img

def preProcessingImgAligner(img):
    predictor_path = "./imgAlignModel/shape_predictor_5_face_landmarks.dat"

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(predictor_path)
    fa = FaceAligner(predictor, desiredFaceWidth=256)

    image = imutils.resize(img, width=800)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    rects = detector(gray, 2)
    try:
        for rect in rects:
            (x, y, w, h) = face_utils.rect_to_bb(rect)
            faceAligned = fa.align(image, gray, rect)
            break
        return faceAligned
    except:
        return img

# 儲存USER向量資料及房間資料進資料庫
def insert2DB(vect, roomId, userNm):
    userId = uuid.uuid4().hex

    cnx = mysql.connector.connect(
        host = host,
        port = port,
        database = database,
        user = user,
        password = password
    )

    # Get a cursor
    cur = cnx.cursor()

    # Execute a query
    cur.execute(f"INSERT INTO MYDB.USER(USERID, USER_NM, FACE_VECT) VALUES('{userId}', '{userNm}', '{vect}')")

    cnx.commit()
    
    cur.execute(f"INSERT INTO MYDB.ROOM(USERID, ROOMID, CRE_DT, END_DT) VALUES('{userId}', '{roomId}', sysdate(), sysdate())")
   
    cnx.commit()

    # Close connection
    cnx.close()

# 讀取資料庫中人臉向量資料
def loadFromDB(roomId):
    cnx = mysql.connector.connect(
        host = host,
        port = port,
        database = database,
        user = user,
        password = password
    )

    # Get a cursor
    cur = cnx.cursor()

    # Execute a query
    cur.execute(f"SELECT `USER_NM`,`FACE_VECT` FROM `user` INNER JOIN room on user.USERID = room.USERID WHERE ROOMID LIKE '{roomId}'")
    result_vect = cur.fetchall()
    vectdict = {}
    for user_vect in result_vect:
        vect_value = np.asarray(eval(user_vect[:][1]))
        vectkeyvalue = {user_vect[:][0]:vect_value}
        vectdict.update(vectkeyvalue)
    # Close connection
    cnx.close()
    
    return vectdict

# 取得最小歐式距離
def Odist(emb_array, vectlist):
    imgVect1 = emb_array
    vectlist = vectlist
    distance = {}
    if len(vectlist) == 0:
        finaldata = {
            'finalname': 'NOMAN',
            'finaldist': 10
        }
        return finaldata
    else :
        for N, D in vectlist.items():
            imgVect2 = D
            dist = np.sqrt(np.sum(np.square(imgVect1-imgVect2)))
            print(f'512維特徵向量的歐氏距離 : {dist}')
            # with open('../../public/307othervect.txt', 'a') as f:
            #     f.write(f'512維特徵向量的歐氏距離 : {dist}\n')
            #     f.close()
            distance.update({N:dist})
        finalname = min(distance, key=distance.get)
        finaldist = min(distance.values())
        finaldata = {
            'finalname': finalname,
            'finaldist': finaldist
        }
        return finaldata

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)