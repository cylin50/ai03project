#!/usr/bin/env python
# coding: utf-8

# In[6]:


import os
import shutil
import time


# In[7]:


from playsound import playsound
import speech_recognition as sr
import wave


# In[11]:


def translate():
    paths = os.listdir('./standby/')
    result = ''
    r = sr.Recognizer()
    for path in paths:
            print ('處理檔案:'+path)
            file = sr.AudioFile('./standby/'+path)
            if os.path.exists('./standby/'+path):
                with file as source:
                    audio = r.record(source)
                    try:
                        text = r.recognize_google(audio,language="zh-tw")
                        result += (text + '，')
                        print(text)
                    except:
                        print('error')
                # shutil.copy("./standby/"+path,'./finish/')
                os.remove("./standby/"+path)
    return (result[0:-1] + '。')


# In[ ]:




