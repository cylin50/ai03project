import sys 
from cv2 import cv2 as cv2
import base64
import face_recognition
import numpy as np
import argparse

imagePath = sys.argv[1]

image = cv2.imread("../../public/images/test.jpg", cv2.COLOR_BGR2RGB)
# image = cv2.imread("C:/Gary/Work/workspace/ai03project/public/images/test.jpg", cv2.COLOR_BGR2RGB)
frame, rectangle = preProcessingImg(image)
cv2.imwrite('../../public/images/test2.jpg',frame)
# retval, buffer = cv2.imencode('.jpg', frame)
# jpg_as_text = base64.b64encode(buffer)

print(str(rectangle))
sys.stdout.flush()

def preProcessingImg(img):
    image = img.copy()
    face_locations = face_recognition.face_locations(image)
    face_locations = np.asarray(face_locations)
    # print(face_locations.shape)
    if len(face_locations) == 0:
        return img
    arg = np.argmax((face_locations[:, 2] - face_locations[:, 0]) * (face_locations[:, 1] - face_locations[:, 3]))
    (top, right, bottom, left) = face_locations[arg]
    
    cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 2)

    rectangle = {}
    rectangle.top = top
    rectangle.right = right
    rectangle.bottom = bottom
    rectangle.left = left
    
    return image, rectangle