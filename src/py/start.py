from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
)
import json
import requests
app = Flask(__name__)

line_bot_api = LineBotApi('Hk/5EujceRnssLuOz3d15x5D4cUk/pJdsklyUzGNbuVpGmlXrW8j3Cc92YbS4TqXduZ7tfFc7MX2KbasgBbIOxZIH2bbSg6ZkLqQ2PFgulj94sP6yN86ogBHbvDz5f9B/M53IK/ANpZJ2dqnAS1KhgdB04t89/1O/w1cDnyilFU=')
handler = WebhookHandler('7c663f2e012421d895115922aa96139a')


@app.route("/api/LineLUIS", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        print("Invalid signature. Please check your channel access token/channel secret.")
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    link = f'https://southeastasia.api.cognitive.microsoft.com/luis/v2.0/apps/fb09ca17-17ec-4c17-8c7b-564bded5c6a2?verbose=true&timezoneOffset=0&subscription-key=b58d019b29734a61b825c8e03fdb8fb2&q={event.message.text}'
    response = requests.get(link)
    answer = response.json()
    
    replytoClient = f'{answer["topScoringIntent"]["intent"]} \n {answer["topScoringIntent"]["score"]}'
    line_bot_api.reply_message(
        event.reply_token,        
        TextSendMessage(text=replytoClient))


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)