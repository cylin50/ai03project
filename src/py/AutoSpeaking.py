#!/usr/bin/env python
# coding: utf-8

import speech_recognition
import tempfile
from gtts import gTTS
from pygame import mixer
from flask import Flask, request, abort
import requests
from playsound import playsound

mixer.init()

inputoutput = {
  '請問飯店的營業時間':'本飯店二十四小時營業',
  '飯店的營業時間':'本飯店二十四小時營業',
  '飯店營業時間':'本飯店二十四小時營業',
  '請問餐廳營業到什麼時候':'餐廳的營業時間是上午十一點到下午兩點，晚上六點到晚上九點',
  '餐廳營業到什麼時候':'餐廳的營業時間是上午十一點到下午兩點，晚上六點到晚上九點',
  '請問餐廳營業到幾點':'餐廳的營業時間是上午十一點到下午兩點，晚上六點到晚上九點',
  '餐廳的營業時間':'餐廳的營業時間是上午十一點到下午兩點，晚上六點到晚上九點',
  '餐廳開到幾點':'餐廳的營業時間是上午十一點到下午兩點，晚上六點到晚上九點',
  '餐廳營業到幾點':'餐廳的營業時間是上午十一點到下午兩點，晚上六點到晚上九點',
  '請問餐廳開到幾點':'餐廳的營業時間是上午十一點到下午兩點，晚上六點到晚上九點',
  '餐廳營業時間':'餐廳的營業時間是上午十一點到下午兩點，晚上六點到晚上九點',
  '請問餐廳的營業時間':'餐廳的營業時間是上午十一點到下午兩點，晚上六點到晚上九點',
  '餐廳怎麼走':'餐廳位於本飯店二樓',
  '請問餐廳怎麼走':'餐廳位於本飯店二樓',
  '餐廳在哪':'餐廳位於本飯店二樓',
  '餐廳在什麼地方':'餐廳位於本飯店二樓',
  '請問餐廳在什麼地方':'餐廳位於本飯店二樓',
  '請問餐廳在哪':'餐廳位於本飯店二樓',
  '請問餐廳在哪裡':'餐廳位於本飯店二樓',
  '餐廳在哪裡':'餐廳位於本飯店二樓',
  '請問健身房營業到幾點':'健身房的營業時間是早上十點到晚上十點',
  '請問健身房開到幾點':'健身房的營業時間是早上十點到晚上十點',
  '健身房開到幾點':'健身房的營業時間是早上十點到晚上十點',
  '健身房的營業時間':'健身房的營業時間是早上十點到晚上十點',
  '健身房營業時間':'健身房的營業時間是早上十點到晚上十點',
  '請問健身房的營業時間':'健身房的營業時間是早上十點到晚上十點',
  '健身房在哪':'健身房位於本飯店三樓',
  '請問健身房在哪':'健身房位於本飯店三樓',
  '請問健身房在哪裡':'健身房位於本飯店三樓',
  '健身房在哪裡':'健身房位於本飯店三樓',
  '請問健身房在什麼地方':'健身房位於本飯店三樓',
  '健身房在什麼地方':'健身房位於本飯店三樓',
  '請問健身房怎麼走':'健身房位於本飯店三樓',
  '健身房怎麼走':'健身房位於本飯店三樓',
  '游泳池在什麼地方':'游泳池位於本飯店十一樓',
  '請問游泳池在什麼地方':'游泳池位於本飯店十一樓',
  '游泳池在哪':'游泳池位於本飯店十一樓',
  '請問游泳池在哪':'游泳池位於本飯店十一樓',
  '請問游泳池在哪裡':'游泳池位於本飯店十一樓',
  '游泳池在哪裡':'游泳池位於本飯店十一樓',
  '游泳池營業到幾點':'游泳池的營業時間是早上十點到晚上十點',
  '請問游泳池營業到幾點':'游泳池的營業時間是早上十點到晚上十點',
  '游泳池的營業時間':'游泳池的營業時間是早上十點到晚上十點',
  '請問游泳池的營業時間':'游泳池的營業時間是早上十點到晚上十點',
  '飯店有什麼設施':'本飯店有餐廳、游泳池、健身房、禮品店、空中花園等',
  '你們有什麼設施':'本飯店有餐廳、游泳池、健身房、禮品店、空中花園等',
  '飯店設施':'本飯店有餐廳、游泳池、健身房、禮品店、空中花園等',
  '請問飯店有什麼設施':'本飯店有餐廳、游泳池、健身房、禮品店、空中花園等',
  '測試':'測試測試'
}

def listenTo():
    r = speech_recognition.Recognizer()

    with speech_recognition.Microphone() as source:
        r.adjust_for_ambient_noise(source)
        audio = r.listen(source)

    return r.recognize_google(audio, language='zh-TW')

def speak(sentence):
#filename= '錄音檔的存取路徑(不再存為temp)
    filename = '../../public/audio_files/test.mp3'
    with open(filename, 'w'):
        tts = gTTS(text=sentence, lang='zh-tw')
        tts.save(filename)
        #於Python撥放錄音
       #  mixer.music.load(filename)
       #  mixer.music.play()
    # while 1:
    #     if not mixer.music.get_busy():
    #         mixer.music.unload()
    #         break

def read():
    listenTo_Response = listenTo()
    print('listenTo_Response: ' + listenTo_Response)
    #.get(更換成你的LUIS模型的URL)
    response = requests.get(f'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/72feab4a-da19-4889-9238-6f912f2f8817?verbose=true&timezoneOffset=0&subscription-key=ddf3b54ff7f14ad99c79c97352c57bce&q={listenTo_Response}')
    answer = response.json()
    print(f'{answer["topScoringIntent"]["intent"]}\n{answer["topScoringIntent"]["score"]}')
    
    speak(inputoutput.get(listenTo_Response, '聽不太清楚，請再說一遍，謝謝'))
    return inputoutput.get(listenTo_Response, '聽不太清楚，請再說一遍，謝謝')



# playsound("/public/audio_files/test.mp3")





# filename = 'audio_files/test.mp3'
#     with open(filename, 'w'):
#         tts = gTTS(text=sentence, lang='zh-tw')
#         tts.save(filename)
#         print('ok')
#         mixer.music.load(filename)
#         mixer.music.play()






