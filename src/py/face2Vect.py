import sys 
import tensorflow as tf
import numpy as np
import scipy.misc
from cv2 import cv2 as cv2
import facenet
import face_recognition
import imutils
import json


def getImgVect(img):
    image_size = 160 
    modeldir = './src/py/facenetModel/20180402-114759.pb' 
    
    tf.Graph().as_default()
    sess = tf.Session()

    facenet.load_model(modeldir)
    images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
    embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
    phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
    embedding_size = embeddings.get_shape()[1]

    scaled_reshape = []
    image = cv2.resize(img, (image_size, image_size), interpolation=cv2.INTER_CUBIC)
    image = facenet.prewhiten(image)
   
    scaled_reshape.append(image.reshape(-1,image_size,image_size,3))
    emb_array = np.zeros((1, embedding_size))
    emb_array[0, :] = sess.run(embeddings, feed_dict={images_placeholder: scaled_reshape[0], phase_train_placeholder: False })[0]
    
    return emb_array[0]

# 圖片前處理
def preProcessingImg(img, rectangle):
    image = img.copy()
    
    top = int(rectangle['top'])
    bottom = int(rectangle['bottom'])
    left = int(rectangle['left'])
    right = int(rectangle['right'])
    
    
    crop_img = image[top:bottom, left:right]
    
    return crop_img

imagePath = sys.argv[1]
rectangle = json.loads(sys.argv[2])

image = cv2.imread(imagePath, cv2.COLOR_BGR2RGB)
image = preProcessingImg(image, rectangle)
imgVect = getImgVect(image)
data = ",".join([str(x) for x in imgVect])
result = "[" + data + "]"

print(result)
sys.stdout.flush()