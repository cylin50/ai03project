from cv2 import cv2 as cv2
import face_recognition
import numpy as np
import os
import json 
import base64

def preProcessingImg(img):
#     print('Start preProcessingImg')
    image = img.copy()
    face_locations = face_recognition.face_locations(image)
    face_locations = np.asarray(face_locations)
    if len(face_locations) == 0:
        return img, ''
    arg = np.argmax((face_locations[:, 2] - face_locations[:, 0]) * (face_locations[:, 1] - face_locations[:, 3]))
    (top, right, bottom, left) = face_locations[arg]
    bottom += 10
    
    cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 2)
    rectangle = {
        "top" : str(top), 
        "right" : str(right),
        "bottom" : str(bottom),
        "left" : str(left),
    }
    
    return image, rectangle

def img2Base64(img):
    retval, buffer = cv2.imencode('.jpg', img)
    jpg_as_text = base64.b64encode(buffer)

    return str(jpg_as_text, encoding='utf-8')

# 選擇攝影機
cap = cv2.VideoCapture(0)

while(True):
    # 從攝影機擷取一張影像
    ret, frame = cap.read()
    frame = cv2.resize(frame, (300, 300))
    frame2, rectangle = preProcessingImg(frame)
    
    b64Image1 = img2Base64(frame)
    b64Image2 = img2Base64(frame2)
    
    result = {
        "image1" : b64Image1,
        "image2" : b64Image2,
        "rectangle" : rectangle
    }
    # 儲存資料
    with open('../../public/images/data.txt', 'w') as f:
        f.write(json.dumps(result))
    # 顯示圖片
    cv2.imshow('frame', frame2)
    
    # 若按下 q 鍵則離開迴圈
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# 釋放攝影機
cap.release()

# 關閉所有 OpenCV 視窗
cv2.destroyAllWindows()