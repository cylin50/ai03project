#!/usr/bin/env python
# coding: utf-8

# In[1]:


from sys import byteorder
from array import array
from struct import pack
import pyaudio
import wave
import datetime
import time
import os


# In[12]:


THRESHOLD = 500
CHUNK_SIZE = 1024
FORMAT = pyaudio.paInt16
RATE = 44100


# In[13]:


def is_silent(snd_data):
    "Returns 'True' if below the 'silent' threshold"
    return max(snd_data) < THRESHOLD


# In[14]:


def normalize(snd_data):
    "Average the volume out"
    MAXIMUM = 16384
    times = float(MAXIMUM)/max(abs(i) for i in snd_data)

    r = array('h')
    for i in snd_data:
        r.append(int(i*times))
    return r


# In[15]:


def trim(snd_data):
    "Trim the blank spots at the start and end"
    def _trim(snd_data):
        snd_started = False
        r = array('h')

        for i in snd_data:
            if not snd_started and abs(i)>THRESHOLD:
                snd_started = True
                r.append(i)

            elif snd_started:
                r.append(i)
        return r
# Trim to the left
    snd_data = _trim(snd_data)

    # Trim to the right
    snd_data.reverse()
    snd_data = _trim(snd_data)
    snd_data.reverse()
    return snd_data


# In[16]:


def add_silence(snd_data, seconds):
    "Add silence to the start and end of 'snd_data' of length 'seconds' (float)"
    silence = [0] * int(seconds * RATE)
    r = array('h', silence)
    r.extend(snd_data)
    r.extend(silence)
    return r


# In[17]:


def record():
    """
    Record a word or words from the microphone and 
    return the data as an array of signed shorts.

    Normalizes the audio, trims silence from the 
    start and end, and pads with 0.5 seconds of 
    blank sound to make sure VLC et al can play 
    it without getting chopped off.
    """
    p = pyaudio.PyAudio()
    stream = p.open(format=FORMAT, channels=1, rate=RATE,
        input=True, output=True,
        frames_per_buffer=CHUNK_SIZE)

    num_silent = 0
    snd_started = False

    r = array('h')
    print('01')
    while 1:
        # little endian, signed short
        snd_data = array('h', stream.read(CHUNK_SIZE))
        if byteorder == 'big':
            snd_data.byteswap()
        r.extend(snd_data)
#         print(type(snd_data))
#         print(snd_data)
        #TRUE :沒有人聲
        silent = is_silent(snd_data)
        # print(f'silent:{silent}, snd_started:{snd_started}, max(snd_data):{max(snd_data)}')
        num_silent += 1
        if not silent and not snd_started:
            snd_started = True
            num_silent = 0 
        elif not silent:
            # print('歸0')
            num_silent = 0 
            
        # 沒聲音且停頓超過100 (結束)
        if not snd_started and num_silent > 100:
            # print('結束')
            return 0, r
        
        # 有聲音停頓超過10 進行切檔案儲存
        if snd_started and num_silent > 10:
            break
            
    # print('02')
    sample_width = p.get_sample_size(FORMAT)
    stream.stop_stream()
    stream.close()
    p.terminate()

    r = normalize(r)
    r = trim(r)
    r = add_silence(r, 0.5)
    return sample_width, r


# In[18]:


def record_to_file():
    "Records from the microphone and outputs the resulting data to 'path'"
    print('錄音開始')
    path = time.strftime("%Y%m%d-%H%M%S")
    i = 0
    while 1:
        sample_width, data = record()
        if sample_width == 0:
            break
            
        data = pack('<' + ('h'*len(data)), *data)
        wf = wave.open('./standby/'+ path + str(i) + '.wav', 'wb')
        i+=1
        wf.setnchannels(1)
        wf.setsampwidth(sample_width)
        wf.setframerate(RATE)
        wf.writeframes(data)
        wf.close()
    print('錄音結束')


# In[20]:





# In[ ]:




