const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');
const http = require('http');
/**GET */
router.get('/page01', (req, res) => {
    res.render('f02/page01');
});

/**Post */
/**範例 */
router.post('/AutoSpeaking', (req, res) => {
    console.log('AutoSpeaking');
    /**設定/讀取/參數 */
    let resend = {};
    /**參數判斷 */

    var options = {
        host: '127.0.0.1',
        port: 5001,
        path: '/api/AutoSpeaking',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
    };

    api = http.request(options, function (resData) {
        console.log('STATUS: ' + resData.statusCode);
        console.log('HEADERS: ' + JSON.stringify(resData.headers));
        resData.setEncoding('utf8');
        resData.on('data', function (chunk) {
            // console.log('BODY: ' + chunk);
            resend.voicestr = chunk.toString();
        });
        resData.on('end', () => {
            resend.code = 200;
            res.json(resend);
        });
    });

    api.write("");

    api.end();
});

module.exports = router;