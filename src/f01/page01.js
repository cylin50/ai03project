const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');
const fs = require("fs");
const querystring = require('querystring');
const http = require('http');

/**GET */
router.get('/page01', (req, res) => {
    res.render('f01/page01');
});

/**Post */
router.post('/checkFaceImg', (req, res) => {
    console.log('checkFaceImg');
    /**設定/讀取/參數 */
    const postData = req.body;
    let resend = {
    };

    /**參數判斷 */
    let base64Img = postData.img;
    let rectangle = postData.rectangle;
    let roomId = postData.roomId;
    let userNm = postData.userNm;
    
    // fs.writeFile("./public/images/face.jpg", new Buffer(base64Img, "base64"), function(err) {console.log(err)});

    const sendData = querystring.stringify({
        // 'imagePath': '../../public/images/face.jpg',
        'base64_str' : base64Img,
        'rectangle': rectangle,
        'roomId' : roomId,
        'userNm' : userNm
    });

    console.log(sendData)

    var options = {
        host: '127.0.0.1',
        port: 5001,
        path: '/api/face2vect',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(sendData)
        }
    };
      
    api = http.request(options, function(resData) {
        console.log('STATUS: ' + resData.statusCode);
        console.log('HEADERS: ' + JSON.stringify(resData.headers));
        resData.setEncoding('utf8');
        resData.on('data', function (chunk) {
            // console.log('BODY: ' + chunk);
            resend.imageVect = chunk.toString();
        });
        resData.on('end', () => {
            resend.code = 200;
            res.json(resend);
        });
    });

    api.write(sendData);

    api.end();
});

/**
 * 取得顧客房號清單(Table)
 */
router.post('/getUserData', (req, res) => {
    console.log('getUserData');
    const postData = req.body;
    let params = [];
    
    let sql1 = `SELECT A.USER_NM, 
                       A.FACE_VECT,
                       B.ROOMID
                    FROM MYDB.USER A 
                    INNER JOIN MYDB.ROOM B ON A.USERID = B.USERID`;
    db.queryAsync(sql1, params)
        .then(result => {
            res.json(result);
        });
});

module.exports = router;