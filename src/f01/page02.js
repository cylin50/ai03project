const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');
const fs = require("fs");
const querystring = require('querystring');
const http = require('http');

/**GET */
router.get('/page02', (req, res) => {
    res.render('f01/page02');
});

/**Post */
router.post('/unlockFaceImg', (req, res) => {
    console.log('unlockFaceImg');
    /**設定/讀取/參數 */
    const postData = req.body;
    let resend = {
    };

    /**參數判斷 */
    let base64Img = postData.img;
    let rectangle = postData.rectangle;
    let roomId = postData.roomId;

    const sendData = querystring.stringify({
        'base64_str' : base64Img,
        'rectangle': rectangle,
        'roomId' : roomId,
    });

    // console.log(sendData)

    var options = {
        host: '127.0.0.1',
        port: 5001,
        path: '/api/faceUnlock',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(sendData)
        }
    };
      
    api = http.request(options, function(resData) {
        console.log('STATUS: ' + resData.statusCode);
        console.log('HEADERS: ' + JSON.stringify(resData.headers));
        resData.setEncoding('utf8');
        resData.on('data', function (chunk) {
            console.log('BODY: ' + chunk);
            resend.dist = chunk.toString();
            console.log('resend.dist: ' + resend.dist);
        });
        resData.on('end', () => {
            resend.code = 200;
            res.json(resend);
        });
    });

    api.write(sendData);

    api.end();
});

module.exports = router;